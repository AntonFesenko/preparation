package com.example.letscode.sarafan.domain;

public final class Views {
    public interface Id{}
    //показує IdName - текст, та Id
    public interface IdName extends Id{}
    //показує FullMessage - текст, та Id(FullMessage extends Id)
    public interface FullMessage extends Id{}

}
